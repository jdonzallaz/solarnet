from cloudpathlib import CloudPath, S3Client
from typing import List
from dataclasses import dataclass


@dataclass
class S3PretrainedModel:
    name: str
    markdown: str


def get_models() -> List[S3PretrainedModel]:
    models: List[S3PretrainedModel] = []
    c = S3Client(endpoint_url="https://k8s-minio.isc.heia-fr.ch:9003")
    r = CloudPath("s3://solarnet-models", c)
    model_names = [f.name for f in r.glob('*') if f.is_dir()]

    for m in model_names:
        md_file = r / m / "README.md"
        if md_file.exists():
            md = md_file.read_text()
        else:
            md = "No readme"
        models.append(S3PretrainedModel(m, md))

    return models


def build_model_page(models: List[S3PretrainedModel]):
    body = """\
# Models
"""
    for m in models:
        body += "\n\n"
        body += m.markdown
    body = body.replace("\n#", "\n##")

    return body


models = get_models()
body = build_model_page(models)

with open("docs/models.md", "w") as text_file:
    text_file.write(body)

for m in models:
    with open(f"docs/models/{m.name}.md", "w") as text_file:
        text_file.write(m.markdown)
