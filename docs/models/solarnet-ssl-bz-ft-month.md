# SolarNet SSL Bz finetuned with SDO-Dataset "month"

## Description

Pre-trained model on solar image data (magnetogram). Pre-trained on SDO-Dataset with data from 2010 to 2018, using only months January to October, and finetuned with the same data. Input is a 2D tensor / 1-channel image (SDO/HMI Bz). Output is a 2-dim tensor representing the prediction (no-flare / >=C flare in the next 24 hours). Architecture is a ResNet-50 CNN pre-trained using self-supervised learning task with SimCLR, and finetuned with a standard MLP head.

It is already finetuned and can be used as-is with similar data, or further finetuned.

## Config

```yaml
id: solarnet-ssl-bz-ft-month
model: ImageClassification
output_size: 2

data:
  name: sdo-benchmark
  channel: bz
  size: 128
model:
  backbone: resnet50
  learning_rate: 2e-05
  hidden_mlp: 2048
  feat_dim: 64
  optimizer: adam
trainer:
  epochs: 50
  batch_size: 128
```

## Shape

- Input shape: `torch.Size([1, 128, 128])`
- Output shape of backbone: `torch.Size([2])`

## Usage

```python
model = ImageClassification.from_pretrained("solarnet-ssl-bz-ft-month")
```

## Author

Jonathan Donzallaz <[jonathan.donzallaz@hefr.ch](mailto:jonathan.donzallaz@hefr.ch)>
