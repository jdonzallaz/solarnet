# SolarNet SSL Bz 2010-2014

## Description

Pre-trained model on solar image data (magnetogram). Trained on SDO-Dataset with data from 2010 to 2014. Input is a 2D tensor / 1-channel image (SDO/HMI Bz). Output is a 2048-dim tensor representation of the image. Architecture is a ResNet-50 CNN pre-trained using self-supervised learning task with SimCLR.

It can be finetuned on any downstream task by adding a classification head or any kind of network.

## Config

```yaml
id: solarnet-ssl-bz-2010-2014
model: SimCLR
backbone_output_size: 2048

data:
  name: sdo-dataset
  channel: bz
  size: 128
model:
  backbone: resnet50
  learning_rate: 0.001
  hidden_mlp: 2048
  feat_dim: 64
  optimizer: adam
trainer:
  epochs: 1000
  batch_size: 768
```

## Shape

- Input shape: `torch.Size([1, 128, 128])`
- Output shape of backbone: `torch.Size([2048])`

## Usage

```python
model = ImageClassification.from_pretrained("solarnet-ssl-bz-2010-2014")
model = SimCLR.from_pretrained("solarnet-ssl-bz-2010-2014")
```

## Author

Jonathan Donzallaz <[jonathan.donzallaz@hefr.ch](mailto:jonathan.donzallaz@hefr.ch)>
