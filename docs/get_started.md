# Get Started

## Installation

```sh
pip install solarnet-lib
```

Python 3.6+ is required.

## Data

Two datasets are supported: SDO-Dataset and SDO-Benchmark. SDO-Dataset needs to be downloaded from the Stanford's servers.
The SDO-Benchmark dataset can be downloaded using the CLI:

```sh
solarnet download sdo-benchmark data/
```

## Dataset

Load a Pytorch dataset:

```python
path = Path("data") / "sdo-benchmark" / "train"
dataset = SDOBenchmarkDataset(path)
```

Also available as a pytorch-lightning datamodule.

```py
path = Path("data") / "sdo-benchmark"
datamodule = SDOBenchmarkDatamodule(path)
```

## Model

Download and load a pre-trained model. It is a Pytorch-Lightning/Pytorch model

```py
model = ImageClassification.from_pretrained("solarnet-ssl-bz-ft-sdo-benchmark")
```

### Make predictions

```py
with torch.no_grad():
    y_pred = model(tensor_of_data)
```

## Finetuning

```py
trainer = pl.Trainer(
    max_epochs=10,
    gpus=0,
)
trainer.fit(model, datamodule=datamodule)
```
