# SDOBenchmark

> SDOBenchmark is a machine learning image dataset for the prediction of solar flares.

The content is well explained on the website.

- 4 GB
- 8336 training samples
- 886 test samples
- Each sample contains 4 time steps. Each time step has 10 different image types, in 256x256 (8 AIA, 2 HMI).
- Labelled with peak emission flux in next 24 hours.


## References

- Website: [https://i4ds.github.io/SDOBenchmark/](https://i4ds.github.io/SDOBenchmark/)
- Repository: [https://github.com/i4Ds/SDOBenchmark](https://github.com/i4Ds/SDOBenchmark)


## How to get it?

Download it from the website or use the solarnet CLI.

```sh
solarnet download sdo-benchmark
```
