# SDO-Dataset

This dataset is from "A Machine Learning Dataset Prepared From the NASA Solar Dynamics Observatory Mission" publication.
It can be used for pre-training with self-supervised learning. We provide a CLI to further prepare the dataset.

All details are available in the publication.

SolarNet is thought to work with the SDOML v1 version of the dataset,
although [a new version](https://sdoml.github.io/#/releases?id=version-20) as been published.

- 12 TB
- ~7.5 mio images from AIA and HMI
- .npz format (Numpy), 512x512 arrays
- Curated and corrected

## References

- Website: [https://sdoml.github.io/](https://sdoml.github.io/)
- Repository: [https://github.com/SDOML/SDOML](https://github.com/SDOML/SDOML)
- Publication: Galvez, Richard, et al. "A Machine-learning Data Set Prepared from the NASA Solar Dynamics Observatory
  Mission." Astrophys. J. Suppl. Ser., vol. 242, no. 1, 8 May. 2019, p. 7, doi:10.3847/1538-4365/ab1005.

## How to get it?

Download it from [the stanford servers](https://purl.stanford.edu/vk217bh4910). We provide
two [scripts](https://gitlab.com/jdonzallaz/solarnet/-/blob/master/scripts/) for easier batch-download in the
repository.
