# SolarNet

The SolarNet library is a deep learning research toolbox focusing on self-supervised learning on solar data. It is part of the SolarNet project, which is
being presented at the [ML-Helio](https://ml-helio.github.io/) conference. The publication will be available shortly.

The SolarNet library permits to use the different Pytorch models, datasets, preprocessing, and others utilities
developed during the SolarNet project. It also makes straightforward the download, loading, and finetuning of the big
pretrained SSL models. With SolarNet, anyone can access the datasets (at this time, only SDO-Benchmark is available to download
through the library) and finetune the powerful models. The library is compatible with Pytorch-Lightning, but the models
are also pure Pytorch Module and the training loop can be written from scratch.

An application serving models and making live predictions is available
at [solarnet.kube.isc.heia-fr.ch](https://solarnet.kube.isc.heia-fr.ch). It uses
the [SSL model finetuned on SDO-Dataset "month"](https://jdonzallaz.gitlab.io/solarnet/models/solarnet-ssl-bz-ft-month/)
.

Find the docs on [jdonzallaz.gitlab.io/solarnet](https://jdonzallaz.gitlab.io/solarnet/).

## Current state and contributions
The library is a work in progress, and the documentation will be completed step-by-step.

You're welcome to contribute and provide feedbacks on Gitlab or by contacting me.


## Get started

### Installation

```sh
pip install solarnet-lib
```

Python 3.6+ is required.

### Data

Two datasets are supported: SDO-Dataset and SDO-Benchmark. SDO-Dataset needs to be downloaded from the Stanford's servers.
The SDO-Benchmark dataset can be downloaded using the CLI:

```sh
solarnet download sdo-benchmark data/
```

### Dataset

Load a Pytorch dataset:

```python
path = Path("data") / "sdo-benchmark" / "train"
dataset = SDOBenchmarkDataset(path)
```

Also available as a pytorch-lightning datamodule.

```py
path = Path("data") / "sdo-benchmark"
datamodule = SDOBenchmarkDatamodule(path)
```

### Model

Download and load a pre-trained model. It is a Pytorch-Lightning/Pytorch model

```py
model = ImageClassification.from_pretrained("solarnet-ssl-bz-ft-sdo-benchmark")
```

#### Make predictions

```py
with torch.no_grad():
    y_pred = model(tensor_of_data)
```

### Finetuning

```py
trainer = pl.Trainer(
    max_epochs=10,
    gpus=0,
)
trainer.fit(model, datamodule=datamodule)
```

## Deployment

```bash
python setup.py sdist bdist_wheel
twine check dist/*
twine upload dist/*
```


## Other tools
Check out those other tools:

- [SDO-CLI](https://github.com/i4Ds/sdo-cli): A practitioners utility for working with SDO data.
- [SunPy](https://sunpy.org/): The community-developed, free and open-source solar data analysis environment for Python.


## Authors

The SolarNet library was developed by [Jonathan Donzallaz](mailto:jonathan.donzallaz@hefr.ch), researcher at iCoSys institute, HEIA-FR, HES-SO.
Thanks to Jean Hennebert, iCoSys, and André Csillaghy, FHNW, for their contributions.


## References

SolarNet uses data, ideas, or libraries from these projects:

- **SDO-Dataset**: Galvez, Richard, et al. "A Machine-learning Data Set Prepared from the NASA Solar Dynamics Observatory Mission." Astrophys. J. Suppl. Ser., vol. 242, no. 1, 8 May. 2019, p. 7, doi:10.3847/1538-4365/ab1005.
- **SDO**: Pesnell, W. D., B. J. Thompson, and P. C. Chamberlin, (2012) The Solar Dynamics Observatory (SDO), Solar Physics, doi: 10.1007/s11207-011-9841-3.
- **HMI**: Schou, J., et al. "Design and Ground Calibration of the Helioseismic and Magnetic Imager (HMI) Instrument on the Solar Dynamics Observatory (SDO)." Sol. Phys., vol. 275, no. 1-2, Jan. 2012, pp. 229-59, doi:10.1007/s11207-011-9842-2.
- **AIA**: Lemen, James R., et al. "The Atmospheric Imaging Assembly (AIA) on the Solar Dynamics Observatory (SDO)." Sol. Phys., vol. 275, no. 1-2, Jan. 2012, pp. 17-40, doi:10.1007/s11207-011-9776-8.
- **SDOBenchmark**: Michael Aerni, Roman Bolzern. SDOBenchmark | SDOBenchmark - Solar flare prediction image dataset.
- **SimCLR**: Chen, Ting, et al. "A Simple Framework for Contrastive Learning of Visual Representations." arXiv, 13 Feb. 2020, doi:10.48550/arXiv.2002.05709.
- **Pytorch**: Paszke, Adam, et al. "NIPS'19: Proceedings of the 33rd International Conference on Neural Information Processing Systems." PyTorch: an imperative style, high-performance deep learning library. Curran Associates, 8 Dec. 2019, pp. 8026-037, doi:10.5555/3454287.3455008.
- **Sunpy**: The SunPy Community et al. “The SunPy Project: Open Source Development and Status of the Version 1.0 Core Package”. In: The Astrophysical Journal 890 (1 2020), pp. 68–. DOI: 10.3847/1538-4357/ab4f7a.


## License

This project is licensed under the terms of the MIT license.
