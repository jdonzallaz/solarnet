import torch

from solarnet.utils.metrics import tss


# TODO: test other metrics


def test_tss():
    assert tss(10, 0, 10, 0) == 1
    assert tss(0, 10, 0, 10) == -1
    assert tss(10, 10, 0, 0) == 0
    assert tss(10, 10, 10, 10) == 0
